FROM node:10 as client-builder
WORKDIR /opt/app

COPY .git ./.git
COPY client/ ./

RUN npm ci

ARG BUILD_INFO

RUN BASE_URL="/" node build.js --gzip

FROM scratch as server-builder
WORKDIR /opt/app

COPY server/alembic.ini server/wait-for-es server/generate-thumb ./
COPY server/szurubooru/ ./szurubooru/
COPY server/config.yaml.dist ./


FROM joshava/uwsgi-nginx
WORKDIR /cache
RUN apk add --no-cache ffmpeg

WORKDIR /client
COPY --from=client-builder /opt/app/public/ ./

WORKDIR /server
COPY server/requirements.txt ./requirements.txt
RUN apk add --no-cache postgresql-libs jpeg-dev zlib-dev && \
    apk add --no-cache --virtual .build-deps build-base gcc musl-dev postgresql-dev libffi-dev && \
    pip install --no-cache-dir -r ./requirements.txt && \
    apk --purge del .build-deps
COPY --from=server-builder /opt/app/ ./

COPY root/ /
