# szurubooru

Szurubooru is an image board engine inspired by services such as Danbooru,
Gelbooru and Moebooru dedicated for small and medium communities. Its name [has
its roots in Polish language and has onomatopeic meaning of scraping or
scrubbing](http://sjp.pwn.pl/sjp/;2527372). It is pronounced as *shoorubooru*.

Read the [rr-/szurubooru][source] or [original README][readme] for more details.

## Difference between rr-/szurubooru

* Server and client are in single image
* Based on Python 3.7 and Alpine instead on Python 3.6 and Slim
* Support S3 as image storage
* CORS headers on `/api`
* Use uWSGI instead of waitress
* Small CSS changes.

`master` branch is synced with [rr-/szurubooru][source] by GitLab.
You can see the difference [here][diff].

## Usage

There is a Docker image build by GitLab CI, [joshava/szurubooru][docker].
You can check the build process in the [pipelines tab][pipelines].
It is recommend to use the pre-build image, because it takes a long time to build Python packages.

Please read the [original tutorial][install] for original configurations.
Here is the new configurations.

* `S3_DOMAIN`: The should be the bucket domain, `bucket.s3.amazonaws.com`. The content should be public readable (listing is not need).
* `S3_ENDPOINT`: Only needed if you are not using Amazon. `https://sfo2.digitaloceanspaces.com`
* `S3_REGION`: Bucket region.
* `S3_ACCESS_KEY`: Bucket access key.
* `S3_ACCESS_SECRET`: Bucket access secret.

[source]: https://github.com/rr-/szurubooru
[readme]: README-MASTER.md
[diff]: https://gitlab.com/joshava/szurubooru/compare/master...joshuaavalon
[docker]: https://hub.docker.com/r/joshava/szurubooru
[pipelines]: https://gitlab.com/joshava/szurubooru/pipelines
[install]: INSTALL.md
